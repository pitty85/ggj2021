#pragma once

#include "EditorObject.hpp"

#include <jngl/Vec2.hpp>

class VertexGrabber : public EditorObject {
public:
	VertexGrabber() = default; // für cereal
	explicit VertexGrabber(jngl::Vec2 position);

	void draw(const EditorInfo&) const override;

	jngl::Vec2 getPosition() const final;
	void setPosition(jngl::Vec2 position) final;

private:
	jngl::Vec2 position;
};
