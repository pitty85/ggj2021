#include "VertexGrabber.hpp"

#include "EditorInfo.hpp"

#include <jngl.hpp>

VertexGrabber::VertexGrabber(const jngl::Vec2 position) {
	setPosition(position);
}

void VertexGrabber::draw(const EditorInfo& info) const {
	jngl::setColor(255, info.selected ? 255 : 0, 0, info.mouseOver ? 220 : 140);
	jngl::drawEllipse(position.x, position.y, info.grabRadius, info.grabRadius);
	if (info.newVertex) {
		jngl::setColor(0, 255, 0, 140);
		jngl::drawEllipse(info.newVertex->x, info.newVertex->y, info.grabRadius, info.grabRadius);
	}
}

jngl::Vec2 VertexGrabber::getPosition() const {
	return position;
}

void VertexGrabber::setPosition(const jngl::Vec2 position) {
	this->position = position;
}
