#pragma once

#include <jngl.hpp>
#include <optional>

struct EditorInfo {
	double grabRadius = 4;
	bool mouseOver = false;

	// Die Position für eine neue Kopie des EditorObjects
	std::optional<jngl::Vec2> newVertex;

	bool selected = false;
	std::optional<jngl::Vec2> grabStartMousePos;
	std::optional<jngl::Vec2> grabStartVertexPos;
	std::optional<double> grabStartTime;
};
