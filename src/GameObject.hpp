#pragma once

#include "leveleditor/EditorObject.hpp"

#include <jngl/Vec2.hpp>

class b2Body;
class b2Shape;

/// Basisklasse für die physischen Objekte im Spiel
class GameObject : public EditorObject {
public:
	/// true zurückgeben damit das Objekt entfernt wird
	virtual bool step() = 0;

	virtual void draw() const = 0;
	virtual void drawReflection() const;
	void draw(const EditorInfo&) const override;

	virtual jngl::Vec2 getPosition() const override;
	void setPosition(jngl::Vec2 position) override;

	float getRotation() const;
	void setRotation(float radian);

	virtual int getAmount() const;

	virtual double getZIndex() const;

	virtual void onContact(GameObject* other, bool foot_sensor);
	virtual void endContact(GameObject* other, bool foot_sensor);

	int numFootContacts;

protected:
	b2Body* body;
};
