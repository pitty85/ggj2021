#include "Level.hpp"

#include "BorderArea.hpp"
#include "Box.hpp"
#include "ColorSwitch.hpp"
#include "Game.hpp"
#include "leveleditor/EditorObject.hpp"
#include "globals.hpp"
#include "Player.hpp"
#include "PlayerSensor.hpp"

#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/json.hpp>
#include <fstream>
#include <fmt/format.h>
#include <poly2tri/poly2tri.h>

using jngl::Vec2;

Level::Level(Game& game, int levelNr)
: game(game), filename(fmt::format("level{:0>2}.json", levelNr)) {
	std::ifstream ifs(filename);
	if (!ifs) {
		ifs.open(fmt::format("level{:0>2}.json", levelNr - 1));
		if (!ifs) {
			throw std::runtime_error(fmt::format("Couldn't read level \"{}\"!", filename));
		}
	}
	cereal::JSONInputArchive archive(ifs);
	archive >> CEREAL_NVP(borderAreas) >> CEREAL_NVP(editorObjects);
	triangulateBorder();

	bool playerFound = false;
	for (const auto& obj : editorObjects) {
		if (dynamic_cast<Player*>(obj.get())) {
			playerFound = true;
			break;
		}
	}
	if (!playerFound) {
		editorObjects.emplace_back(std::make_unique<Player>(nullptr, 0));
		editorObjects.emplace_back(std::make_unique<Player>(nullptr, 1));
	}
}

Level::~Level() = default;

BorderArea* Level::getBorderAreaAt(Vec2 position) {
	for (auto& borderArea : borderAreas) {
		if (borderArea.contains(position)) {
			return &borderArea;
		}
	}
	return nullptr;
}

void Level::triangulateBorder() {
	for (auto& borderArea : borderAreas) {
		borderArea.create([this](std::vector<Vec2> vertices, std::vector<std::vector<Vec2>> holes,
		                         AreaColor color) {
			return triangulateBorder(std::move(vertices), std::move(holes), color);
		});
	}
}

bool Level::mouseOverEditorObject() const {
	for (const auto& borderArea : borderAreas) {
		if (borderArea.mouseOverVertexGrabber() ||
		    borderArea.contains(game.getAbsoluteMousePos())) {
			return true;
		}
	}
	for (const auto& obj : editorObjects) {
		const auto& it = editorInfos.find(obj.get());
		if (it == editorInfos.end()) {
			continue;
		}
		if (obj->mouseOverGrabber(it->second, game.getAbsoluteMousePos())) {
			return true;
		}
	}
	return false;
}

void Level::stepForEditor(std::optional<std::array<jngl::Vec2, 2>> selectInRectangle) {
	bool changed = false;
	auto toFront = borderAreas.end();
	for (auto it = borderAreas.begin(); it != borderAreas.end(); ++it) {
		if (it->step(multipleSelection, game.getAbsoluteMousePos())) {
			changed = true;
			if (jngl::keyPressed('g')) {
				toFront = it;
			}
		}
	}
	if (toFront != borderAreas.end()) {
		std::swap(*toFront, borderAreas.back());
	}

	std::vector<std::pair<EditorObject*, EditorInfo*>> editorObjectsToStep;
	for (auto& editorObject : editorObjects) {
		editorObjectsToStep.emplace_back(editorObject.get(), &editorInfos[editorObject.get()]);
	}
	for (auto& borderArea : borderAreas) {
		const auto tmp = borderArea.getEditorObjects();
		editorObjectsToStep.insert(editorObjectsToStep.end(), tmp.begin(), tmp.end());
	}
	bool needToSelect = jngl::mousePressed(jngl::mouse::Left);
	std::vector<EditorObject*> needToRemove;
	for (const auto& it : editorObjectsToStep) {
		std::optional<jngl::Vec2> cloneHere;
		switch (
		    it.first->step(cloneHere, multipleSelection, game.getAbsoluteMousePos(), *it.second)) {
		case EditorObject::Action::NEED_TO_SELECT:
			needToSelect = true;
			multipleSelection = false;
			break;
		case EditorObject::Action::CHANGED:
			changed = true;
			break;
		case EditorObject::Action::DELETE_ME:
			needToRemove.emplace_back(it.first);
			break;
		case EditorObject::Action::NOTHING:
			break;
		}
	}
	if (needToSelect || selectInRectangle) {
		for (auto& borderArea : borderAreas) {
			borderArea.setSelected(false);
		}
	}
	size_t selectedEditorObjects = 0;
	for (auto& it : editorObjectsToStep) {
		if (!multipleSelection && it.second->mouseOver && needToSelect) {
			if (!it.second->selected) {
				// Wenn man ein Objekt anklickt, werden die anderen abgewählt
				for (auto& editorObject : editorObjectsToStep) {
					it.second->selected = false;
				}
				it.second->selected = true;
			}
			// Nur ein Objekt muss ausgewählt / soll gleichzeitg verschoben werden:
			needToSelect = false;
		}
		if (selectInRectangle) {
			it.second->selected = (it.first->getPosition().x > (*selectInRectangle)[0].x &&
			                       it.first->getPosition().y > (*selectInRectangle)[0].y &&
			                       it.first->getPosition().x < (*selectInRectangle)[1].x &&
			                       it.first->getPosition().y < (*selectInRectangle)[1].y);
		}
		if (it.second->selected) {
			++selectedEditorObjects;
		}
	}
	if (needToSelect) { // wenn man keinen VertexGrabber getroffen hat
		                // sollen ganze BorderAreas ausgewählt werden können:
		for (auto& borderArea : borderAreas) {
			if (borderArea.contains(game.getAbsoluteMousePos())) {
				borderArea.setSelected(true);
			}
		}
	}
	if (jngl::mousePressed(jngl::mouse::Left)) {
		for (auto& it : editorObjectsToStep) {
			if (it.second->selected) {
				it.first->startDrag(*it.second, game.getAbsoluteMousePos());
			}
		}
	}
	multipleSelection = selectedEditorObjects > 1;
	if (changed) {
		game.triangulateBorder();
	}
	for (const auto toRemove : needToRemove) {
		// Wir schauen nur in editorObjects, da sich die BorderArea um das Löschen von
		// VertexGrabbern kümmert.
		{
			const auto it = std::find_if(editorObjects.begin(), editorObjects.end(),
			                             [toRemove](const auto& p) { return p.get() == toRemove; });
			if (it != editorObjects.end()) {
				editorObjects.erase(it);
			}
		}
	}
}

void Level::save() const {
	std::stringstream tmp;
	{
		cereal::JSONOutputArchive oarchive(tmp);
		oarchive << CEREAL_NVP(borderAreas) << CEREAL_NVP(editorObjects);
	}
	std::ofstream ofs(filename);
	if (!ofs) {
		jngl::errorMessage(fmt::format("Couldn't save level to '{}'!", filename));
		return;
	}
	ofs << tmp.str();
}

void Level::addBorderArea(Vec2 position) {
	std::vector<Vec2> tmp;
	tmp.emplace_back(position);
	std::vector<std::vector<Vec2>> tmp2;
	borderAreas.emplace_back(tmp, tmp2);
}

void Level::drawForEditor() const {
	for (auto& borderArea : borderAreas) {
		borderArea.draw();
	}
	for (const auto& editorObject : editorObjects) {
		const auto& it = editorInfos.find(editorObject.get());
		if (it == editorInfos.end()) {
			continue;
		}
		editorObject->draw(it->second);
	}
}

std::vector<std::array<Vec2, 3>> Level::triangulateBorder(std::vector<Vec2> vertices,
                                                          std::vector<std::vector<Vec2>> holes,
                                                          AreaColor color) {
	std::vector<std::array<Vec2, 3>> rtn;
	if (vertices.size() > 2) {
		std::vector<p2t::Point*> poly;
		{
			const Vec2* previous = &vertices.back();
			for (const auto& vertex : vertices) {
				// TODO: addLine(previous->x, previous->y, vertex.x, vertex.y);
				previous = &vertex;
				poly.push_back(new p2t::Point(vertex.x, vertex.y));
			}
		}
		p2t::CDT cdt(poly);
		for (const auto& hole : holes) {
			if (hole.size() < 3) {
				std::cout << "WARNING: zombie hole detected.\n";
				continue;
			}
			std::vector<p2t::Point*> holePoints;
			const Vec2* previous = &hole.back();
			for (const auto& vertex : hole) {
				// TODO: addLine(previous->x, previous->y, vertex.x, vertex.y);
				previous = &vertex;
				holePoints.push_back(new p2t::Point(vertex.x, vertex.y));
			}
			cdt.AddHole(holePoints);
		}
		cdt.Triangulate();
		std::vector<p2t::Triangle*> triangles(cdt.GetTriangles());
		for (const auto& triangle : triangles) {
			for (const auto addedTriangle :
			     game.addTriangle({ triangle->GetPoint(0)->x, triangle->GetPoint(0)->y },
			                      { triangle->GetPoint(1)->x, triangle->GetPoint(1)->y },
			                      { triangle->GetPoint(2)->x, triangle->GetPoint(2)->y }, color)) {
				rtn.emplace_back(addedTriangle);
			}
		}
	}
	return rtn;
}

void Level::spawnFromEditorObjects() {
	for (auto& editorObject : editorObjects) {
		spawnedFromEditorObjects.push_back(editorObject->spawn());
		game.add(std::unique_ptr<GameObject>(spawnedFromEditorObjects.back()));
		if (auto player = dynamic_cast<Player*>(spawnedFromEditorObjects.back())) {
			// Unter jeden Spieler einen Sensor platzieren
			auto p = std::make_unique<PlayerSensor>(*player);
			spawnedFromEditorObjects.push_back(p.get());
			game.add(std::move(p));
		}
	}
	game.addRemoveObjects();
}

void Level::removeSpawnedFromEditorObjects() {
	for (const auto gameObject : spawnedFromEditorObjects) {
		game.remove(gameObject);
	}
	spawnedFromEditorObjects.clear();
}

std::vector<BorderArea>* Level::getBorderAreas() {
	return &borderAreas;
}
