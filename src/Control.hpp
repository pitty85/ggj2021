#pragma once

#include <jngl/Vec2.hpp>

class Control {
public:
	virtual ~Control() = default;

	/// Richtungsvektor
	virtual jngl::Vec2 getMovement() const = 0;
	virtual bool jump() const = 0;
	virtual bool crouch() const = 0;

	virtual void vibrate();
};
