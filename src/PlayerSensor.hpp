#pragma once

#include "GameObject.hpp"

class Player;

class PlayerSensor : public GameObject {
public:
	explicit PlayerSensor(Player&);
	~PlayerSensor() override;

	bool step() override;
	void draw() const override;

	void onContact(GameObject*, bool foot_sensor) override;
	void endContact(GameObject*, bool foot_sensor) override;

private:
	constexpr static float RADIUS = 2;

	Player& player;
};
