#include "Triangle.hpp"

#include "constants.hpp"
#include "globals.hpp"

#include <box2d/box2d.h>

Triangle::Triangle(b2World& world, std::array<jngl::Vec2, 3> data, AreaColor color)
: data(data), color(color) {
	b2BodyDef bodyDef;
	bodyDef.position = pixelToMeter(data[0]);
	bodyDef.type = b2_kinematicBody;
	body = world.CreateBody(&bodyDef);

	b2PolygonShape shape;
	b2Vec2 points[] = { b2Vec2(0, 0), pixelToMeter(data[1] - data[0]),
		                pixelToMeter(data[2] - data[0]) };
	shape.Set(points, 3);
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff & ~toFilter(color);
	body->CreateFixture(&fixtureDef);
}

Triangle::Triangle(Triangle&& other) noexcept
: data(other.data), body(other.body), color(other.color) {
	other.body = nullptr;
}

Triangle::~Triangle() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

template <class T> float lerp(T a, T b, float f) {
	return a + f * (b - a);
}

void Triangle::draw() const {
	auto c = toColor(color);
	if (g_showBoundingBoxes) {
		c.setRed(uint8_t(lerp(float(c.getRed()), float(uint8_t(data[0].x) % 256), 0.2f)));
		c.setRed(uint8_t(lerp(float(c.getRed()), float(uint8_t(data[0].y) % 256), 0.2f)));
		c.setGreen(uint8_t(lerp(float(c.getGreen()), float(uint8_t(data[1].x) % 256), 0.2f)));
		c.setGreen(uint8_t(lerp(float(c.getGreen()), float(uint8_t(data[1].y) % 256), 0.2f)));
		c.setBlue(uint8_t(lerp(float(c.getBlue()), float(uint8_t(data[2].x) % 256), 0.2f)));
		c.setBlue(uint8_t(lerp(float(c.getBlue()), float(uint8_t(data[2].y) % 256), 0.2f)));
		jngl::setColor(c);
	}
	jngl::setColor(c);
	jngl::drawTriangle(data[0].x, data[0].y, data[1].x, data[1].y, data[2].x, data[2].y);
}
