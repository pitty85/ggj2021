#include "GameObject.hpp"

#include "constants.hpp"
#include "leveleditor/EditorInfo.hpp"

#include <box2d/box2d.h>
#include <jngl.hpp>

void GameObject::drawReflection() const {
}

jngl::Vec2 GameObject::getPosition() const {
	return meterToPixel(body->GetPosition());
}

void GameObject::setPosition(const jngl::Vec2 position) {
	const auto transform = body->GetTransform();
	body->SetTransform(pixelToMeter(position), transform.q.GetAngle());
}

void GameObject::setRotation(const float radian) {
	const auto transform = body->GetTransform();
	body->SetTransform(transform.p, radian);
}

float GameObject::getRotation() const {
	return body->GetTransform().q.GetAngle();
}

int GameObject::getAmount() const {
	return 0;
}

double GameObject::getZIndex() const {
	return getPosition().y;
}

void GameObject::onContact(GameObject* other, bool foot_sensor) {
}

void GameObject::endContact(GameObject* other, bool foot_sensor) {
}

void GameObject::draw(const EditorInfo& info) const {
	jngl::setColor(info.selected ? 220 : 0, 220, 0, info.mouseOver ? 220 : 140);
	jngl::drawEllipse(getPosition().x, getPosition().y, info.grabRadius, info.grabRadius);
}
