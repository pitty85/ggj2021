#pragma once

#include <jngl/work.hpp>
#include <string>

class WinningScreen : public jngl::Work {
public:
	WinningScreen(std::shared_ptr<jngl::Work> game);
	~WinningScreen();
	void step() override;
	void draw() const override;

private:
	double timeScale = 0.0;
    std::string* skin = nullptr;

	int blink = 255;

	std::shared_ptr<jngl::Work> game;

    /// Zählt hoch
    int time = 0;
};
