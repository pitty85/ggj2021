#pragma once

#include "AreaColor.hpp"
#include "GameObject.hpp"
#include "engine/Animation.hpp"

#include <array>
#include <box2d/box2d.h>
#include <cereal/types/polymorphic.hpp>
#include <jngl.hpp>
#include <memory>

struct spSkeletonData;

class b2World;
class Button;
class Control;
namespace spine {
class AnimationState;
class SkeletonDrawable;
} // namespace spine

class Player : public GameObject {
public:
	void createJoint();
	void destroyJoint();
	explicit Player(b2World* world = nullptr, int playerNr = 0);
	~Player() override;

	Player(const Player&) = delete;
	Player& operator=(const Player&) = delete;
	Player(Player&&) = delete;
	Player& operator=(Player&&) = delete;

	bool step() override;

	void draw() const override;
	void draw(const EditorInfo&) const override;

	void onContact(GameObject*, bool foot_sensor) override;
	void endContact(GameObject*, bool foot_sensor) override;

	void createFixtureAndShape();

	void vibrate();

	void onAnimationComplete();

	GameObject* spawn() const override;

	AreaColor getColor();

	void shake();

	/// Ist man auf dem Rücken des anderen?
	bool onAnotherPlayer = false;

	/// wird vom Game gesetzt, wenn beide im Portal sind. Dann vergeht noch etwas zeit bis zum
	/// nächsten Level
	bool won = false;

private:
	bool fallAnimationActive() const;

	b2DistanceJoint* joint1 = nullptr;
	b2DistanceJoint* joint2 = nullptr;
	b2DistanceJoint* joint3 = nullptr;
	b2DistanceJoint* joint4 = nullptr;
	Player* otherPlayer = nullptr;
	bool createTheJoint = false;
	bool destroyTheJoint = false;
	int playerNr = -1;

	// wird ungleichmäßig hochgezählt für die Animation des Kopfs
	float time = 0;

	int dashCountdown = 0;
	b2Vec2 dashDirection;
	bool playerOnTop = false;

	std::unique_ptr<Control> control;

	AreaColor color;

	std::unique_ptr<spine::SkeletonDrawable> skeleton;
	std::unique_ptr<spine::AnimationState> blinkAnimation;
	std::unique_ptr<spine::AnimationState> lookAnimation;
	std::unique_ptr<spine::AnimationState> mouthAnimation;
	std::string currentAnimation = "idle";
	std::string nextAnimation;

	int remainingJumpSteps = 0;
	bool jumpAvailable = true;

	bool crouch = false;

	int lastNumFootContacts = 0;

	/// Wieviele steps die y velocity schon größer 0 ist
	int stepsFalling = 0;

	friend class cereal::access;
	template <class Archive> void save(Archive& ar, const unsigned int) const {
		ar << getPosition() << playerNr;
	}
	template <class Archive> void load(Archive& ar, const unsigned int) {
		jngl::Vec2 position;
		ar >> position >> playerNr;
		color = playerNr == 0 ? AreaColor::RED : AreaColor::BLUE;
		setPosition(position);
	}
};

CEREAL_REGISTER_TYPE(Player)
CEREAL_REGISTER_POLYMORPHIC_RELATION(EditorObject, Player)
