#pragma once

#include "AreaColor.hpp"
#include "leveleditor/EditorInfo.hpp"
#include "Goal.hpp"

#include <array>
#include <box2d/box2d.h>
#include <jngl.hpp>
#include <optional>

class BorderArea;
class EditorObject;
class Game;
class VertexGrabber;

class Level {
public:
	explicit Level(Game&, int levelNr);
	~Level();

	Level(const Level&) = delete;
	Level(Level&&) = delete;
	Level& operator=(const Level&) = delete;
	Level& operator=(Level&&) = delete;

	BorderArea* getBorderAreaAt(jngl::Vec2 position);

	std::vector<BorderArea>* getBorderAreas();

	void triangulateBorder();

	/// Gibt zurück, ob die Maus über mindestens einem EditorObject ist
	bool mouseOverEditorObject() const;

	void stepForEditor(std::optional<std::array<jngl::Vec2, 2>> selectInRectangle);

	void save() const;

	void addBorderArea(jngl::Vec2 position);

	void drawForEditor() const;

	// Erstellt an jedem EditorObject das entsprechende GameObject in Game
	void spawnFromEditorObjects();

	void removeSpawnedFromEditorObjects();

	/// Wird vom LevelEditor aufgerufen, wenn man ein simples Objekt hinzufügt
	template <class T, class... Args> T& add(Args&&... args) {
		auto p = std::make_unique<T>(args...);
		T& rtn = *p;
		editorObjects.emplace_back(std::move(p));
		return rtn;
	}

private:
	std::vector<std::array<jngl::Vec2, 3>>
	triangulateBorder(std::vector<jngl::Vec2> vertices, std::vector<std::vector<jngl::Vec2>> holes,
	                  AreaColor);

	Game& game;

	std::vector<BorderArea> borderAreas;

	/// Alle EditorObjects bis auf die VertexGrabber
	std::vector<std::unique_ptr<EditorObject>> editorObjects;

	/// Zu jedem EditorObject werden hier infos für den Editor gespeichert
	std::unordered_map<EditorObject*, EditorInfo> editorInfos;

	/// Sind im LevelEditor mehrere Vertexes markiert? TODO: Sollte nicht Teil von Level sein.
	bool multipleSelection = false;

	std::vector<GameObject*> spawnedFromEditorObjects;

	std::string filename;
};
