#pragma once

#include "AreaColor.hpp"
#include "leveleditor/EditorInfo.hpp"
#include "leveleditor/VertexGrabber.hpp"

#include <cereal/access.hpp>
#include <cereal/types/vector.hpp>
#include <jngl.hpp>

class BorderArea {
public:
	BorderArea() = default; // für cereal
	BorderArea(std::vector<jngl::Vec2> vertices, std::vector<std::vector<jngl::Vec2>> holes);

	void addHole(jngl::Vec2);

	bool contains(jngl::Vec2) const;

	bool step(bool multipleSelection, jngl::Vec2 globalMousePos);

	void setSelected(bool);

	void draw() const;

	/// Gibt zurück, ob die Maus über mindestens einem VertexGrabber ist
	bool mouseOverVertexGrabber() const;

	/// Übergibt an die übergebene Funktion vertices und holes und erhält von ihr das
	/// Triangulations-Ergebnis zurück
	void create(const std::function<std::vector<std::array<jngl::Vec2, 3>>(
	                std::vector<jngl::Vec2> vertices, std::vector<std::vector<jngl::Vec2>> holes,
	                AreaColor)>&);

	std::vector<std::pair<VertexGrabber*, EditorInfo*>> getEditorObjects();

	void switchRedBlue();

private:
	friend class cereal::access;
	template <class Archive> void serialize(Archive& ar, const unsigned int version) {
		ar& CEREAL_NVP(vertices);
		ar& CEREAL_NVP(holes);
		if (version >= 1) {
			ar& CEREAL_NVP(color);
		}
	}

	std::vector<VertexGrabber> vertices;
	std::vector<std::vector<VertexGrabber>> holes;

	/// Das Ergebnis der Triangulation
	std::vector<std::array<jngl::Vec2, 3>> triangles;

	AreaColor color = AreaColor::RED;

	bool selected = false;

	std::unordered_map<const VertexGrabber*, EditorInfo> vertexInfos;
};

CEREAL_CLASS_VERSION(BorderArea, 1);
