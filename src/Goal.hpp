#pragma once

#include "GameObject.hpp"
#include "leveleditor/EditorObject.hpp"

#include <cereal/types/polymorphic.hpp>
#include <cereal/cereal.hpp>
#include <jngl.hpp>

class b2World;
namespace spine {
class SkeletonDrawable;
} // namespace spine

class Goal : public GameObject {
public:
	Goal();
	~Goal();

	bool step() override;
	void draw() const override;
	void draw(const EditorInfo&) const override;
	GameObject* spawn() const override;
	void onContact(GameObject* other, bool foot_sensor) override;
	void endContact(GameObject* other, bool foot_sensor) override;
	int getNumPlayer();

private:
	const static float RADIUS;
	jngl::Color* color;
	std::unique_ptr<spine::SkeletonDrawable> skeleton;
	float scale = 0.f;

	friend class cereal::access;
	template <class Archive> void save(Archive& ar, const unsigned int) const {
		ar << getPosition();
	}
	template <class Archive> void load(Archive& ar, const unsigned int) {
		jngl::Vec2 position;
		ar >> position;
		setPosition(position);
	}

	GameObject* lastPlayer = nullptr;
	int num_player;
};

CEREAL_REGISTER_TYPE(Goal)
CEREAL_REGISTER_POLYMORPHIC_RELATION(EditorObject, Goal)
