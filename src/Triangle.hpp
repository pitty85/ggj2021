#pragma once

#include "AreaColor.hpp"

#include <array>
#include <jngl.hpp>

class b2Body;
class b2World;

class Triangle {
public:
	explicit Triangle(b2World&, std::array<jngl::Vec2, 3>, AreaColor);
	~Triangle();
	Triangle(const Triangle&) = delete;
	Triangle& operator=(const Triangle&) = delete;
	Triangle&& operator=(Triangle&&) = delete;
	Triangle(Triangle&&) noexcept;

	void draw() const;

private:
	AreaColor color;
	std::array<jngl::Vec2, 3> data;
	b2Body* body;
};
