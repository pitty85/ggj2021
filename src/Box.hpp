#pragma once

#include "GameObject.hpp"
#include "leveleditor/EditorObject.hpp"

#include <cereal/types/polymorphic.hpp>
#include <cereal/cereal.hpp>
#include <jngl.hpp>

class b2World;

class Box : public GameObject {
public:
	Box(b2World* world = nullptr);
	~Box();
	bool step() override;
	void draw() const override;
	void draw(const EditorInfo&) const override;
	GameObject* spawn() const override;

private:
	friend class cereal::access;
	template <class Archive> void save(Archive& ar, const unsigned int) const {
		ar << getPosition();
	}
	template <class Archive> void load(Archive& ar, const unsigned int) {
		jngl::Vec2 position;
		ar >> position;
		setPosition(position);
	}
};

CEREAL_REGISTER_TYPE(Box)
CEREAL_REGISTER_POLYMORPHIC_RELATION(EditorObject, Box)
