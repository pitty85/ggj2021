#include "AnimationState.hpp"

#include "SkeletonDrawable.hpp"

#include <spine/spine.h>

namespace spine {

void AnimationState_AnimationStateListener(spAnimationState* state, spEventType type,
                                           spTrackEntry* entry, spEvent* event) {
	switch (type) {
	case SP_ANIMATION_INTERRUPT:
		break;

	case SP_ANIMATION_COMPLETE:
		if (!entry->loop && !entry->next && entry->userData) {
			reinterpret_cast<AnimationState*>(entry->userData)->onAnimationComplete();
			entry->userData = nullptr;
		}
		break;
	default:
		break;
	}
}

AnimationState::AnimationState(SkeletonDrawable& sd, std::string animationName)
: skeleton(sd.skeleton), data(spAnimationStateData_create(skeleton->data)),
  state(spAnimationState_create(data)), animationName(std::move(animationName)) {
	const auto entry = spAnimationState_setAnimationByName(state, 0, "add_look", 0 /* loop */);
	entry->userData = this;
	entry->listener = AnimationState_AnimationStateListener;
}

AnimationState::~AnimationState() {
	spAnimationStateData_dispose(data);
	spAnimationState_dispose(state);
}

void AnimationState::step() {
	const float deltaTime = 1.f / float(jngl::getStepsPerSecond());
	spAnimationState_update(state, deltaTime);
	spAnimationState_apply(state, skeleton);
}

void AnimationState::onAnimationComplete() {
	const static char* const animations[] = { "add_blink", "add_look" };
	auto entry = spAnimationState_addAnimationByName(state, 0, animations[rand() % 2], 0,
	                                                 2.4f + float(rand() % 3));
	entry->userData = this;
	entry->listener = AnimationState_AnimationStateListener;
}

} // namespace spine
