#pragma once

#include <array>
#include <jngl/Vec2.hpp>

constexpr int GRID_SIZE = 5;

jngl::Vec2 roundToGrid(const jngl::Vec2& vertex);

bool pointInsideTriangle(const jngl::Vec2& s, const std::array<jngl::Vec2, 3>& triangle);
