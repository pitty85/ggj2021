#pragma once

#include <spine/spine.h>
#include <string>

namespace spine {

class SkeletonDrawable;

class AnimationState {
public:
	explicit AnimationState(SkeletonDrawable&, std::string animationName);
	~AnimationState();

	void step();

	void onAnimationComplete();

private:
	spSkeleton* skeleton;
	spAnimationStateData* data;
	spAnimationState* state;
	std::string animationName;
};

} // namespace spine
