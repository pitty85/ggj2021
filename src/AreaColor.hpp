#pragma once

#include <jngl.hpp>

enum class AreaColor {
	BLUE,
	RED,
	PURPLE,
};

jngl::Color toColor(AreaColor);
uint16_t toFilter(AreaColor);
