#include "Goal.hpp"
#include "Player.hpp"

#include "engine/SkeletonDrawable.hpp"
#include "constants.hpp"
#include "globals.hpp"

#include <box2d/box2d.h>
#include <jngl.hpp>

const float Goal::RADIUS = 20;

Goal::Goal() {
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(0, 0);
	bodyDef.type = b2_kinematicBody;
	body = g_world->CreateBody(&bodyDef);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
	body->SetLinearDamping(10.f);
	body->SetGravityScale(0);

	b2CircleShape shape;
	shape.m_radius = RADIUS / PIXEL_PER_METER;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0;
	fixtureDef.restitution = 0.1f;
	fixtureDef.isSensor = true;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_NON_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef)->GetUserData().pointer =
	    reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
	body->SetGravityScale(1);

	num_player = 0;
	color = new jngl::Color(0, 0, 0);

	spAtlas* atlas = spAtlas_createFromFile("portal.atlas", nullptr);
	assert(atlas);
	spSkeletonJson* json = spSkeletonJson_create(atlas);
	json->scale = 0.4;

	const auto skeletonData = spSkeletonJson_readSkeletonDataFile(json, "portal.json");
	assert(skeletonData);
	spSkeletonJson_dispose(json);

	spAnimationStateData* animationStateData = spAnimationStateData_create(skeletonData);
	skeleton = std::make_unique<spine::SkeletonDrawable>(skeletonData, animationStateData);
	spAnimationState_setAnimationByName(skeleton->state, 0, "portal", true);
}

Goal::~Goal() {
	body->GetWorld()->DestroyBody(body);
}

bool Goal::step() {
	skeleton->step();
	scale += 0.06f * (1.f - scale);
	return false;
}

void Goal::draw() const {
	jngl::pushMatrix();
	jngl::translate(getPosition());
	jngl::pushSpriteAlpha(150);
	jngl::setSpriteColor(*color);
	jngl::scale(scale);
	skeleton->draw();
	jngl::setSpriteAlpha(255);
	jngl::popMatrix();
	if (g_showBoundingBoxes) {
		jngl::print(std::to_string(num_player), getPosition());
	}
}

void Goal::draw(const EditorInfo& info) const {
	GameObject::draw(info);
	jngl::print("Ziel", getPosition());
}

GameObject* Goal::spawn() const {
	auto p = new Goal;
	p->setPosition(getPosition());
	return p;
}

void Goal::onContact(GameObject* other, bool foot_sensor) {
	const auto b = dynamic_cast<Player*>(other);
	if (!foot_sensor && b && lastPlayer != b) {
		*color = toColor(b->getColor());
		lastPlayer = b;
		num_player++;
		if (num_player >= 2) {
			*color = toColor(AreaColor::PURPLE);
		}
	}
}

void Goal::endContact(GameObject* other, bool foot_sensor) {
}

int Goal::getNumPlayer(){
	return num_player;
}
