#include "constants.hpp"
#include "Intro.hpp"
#include "Game.hpp"

#include <ctime>
#include <jngl/main.hpp>
#include <jngl/message.hpp>
#include <jngl/input.hpp>
#include <jngl/job.hpp>
#include <jngl/init.hpp>

#ifndef EMSCRIPTEN
	#if (defined(__linux__) && !__has_include(<filesystem>))
	#include <experimental/filesystem>
	namespace fs = std::experimental::filesystem;
	#else
	#include <filesystem>
	namespace fs = std::filesystem;
	#endif
#endif

class QuitWithEscape : public jngl::Job {
public:
	void step() override {
		if (jngl::keyPressed(jngl::key::Escape)) {
			jngl::quit();
		}
	}
	void draw() const override {
	}
};

std::function<std::shared_ptr<jngl::Work>()> jnglInit(jngl::AppParameters& params) {
	std::srand(std::time(nullptr));
	params.displayName = programDisplayName;
	params.screenSize = { 640, 360 };

	return []() {
#ifdef EMSCRIPTEN
	chdir("data");
#else
		auto dataFolder = fs::path(jngl::getBinaryPath()) / fs::path("../data");
		if (!fs::exists(dataFolder)) {
			dataFolder = fs::path(jngl::getBinaryPath()) / fs::path("../../data");
			if (!fs::exists(dataFolder)) {
				dataFolder = fs::path(jngl::getBinaryPath()) / fs::path("data");
			}
		}
		fs::current_path(dataFolder);
#endif
// #ifndef NDEBUG
// 		jngl::addJob(std::make_shared<QuitWithEscape>());
// #endif
		return std::make_shared<Game>(0);
	};
}
