#pragma once

#include <jngl.hpp>

class Button : public jngl::Widget {
public:
	Button(const std::string& label, jngl::Vec2 position, std::function<void()> onClicked,
	       bool bigFont = false);

	Action step() override;
	void drawSelf() const override;

	void setLabel(const std::string&);

private:
	static jngl::Font& font();
	static jngl::Font& bigFont();

	std::function<void()> onClicked;
	jngl::TextLine label;
	bool mouseover = false;
};
