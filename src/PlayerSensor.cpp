#include "PlayerSensor.hpp"

#include "constants.hpp"
#include "globals.hpp"
#include "Player.hpp"

#include <box2d/box2d.h>
#include <jngl.hpp>

PlayerSensor::PlayerSensor(Player& player) : player(player) {
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(0, 0);
	bodyDef.type = b2_dynamicBody;
	body = g_world->CreateBody(&bodyDef);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
	body->SetGravityScale(0);

	b2CircleShape shape;
	shape.m_radius = RADIUS / PIXEL_PER_METER;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0;
	fixtureDef.restitution = 0.1f;
	fixtureDef.isSensor = true;
	fixtureDef.filter.categoryBits = toFilter(player.getColor());
	fixtureDef.filter.maskBits = 0xffff & ~FILTER_CATEGORY_NON_SOLID_OBJECT;
	body->CreateFixture(&fixtureDef);
	body->SetGravityScale(1);
}

PlayerSensor::~PlayerSensor() {
	auto tmp = body;
	body = nullptr;
	tmp->GetWorld()->DestroyBody(tmp); // might call endContact
}

bool PlayerSensor::step() {
	setPosition(player.getPosition() + jngl::Vec2(0, 9));
	return false;
}

void PlayerSensor::draw() const {
	if (!g_showBoundingBoxes) {
		return;
	}
	jngl::setColor(0x00ff00_rgb);
	jngl::setAlpha(200);
	jngl::drawCircle(getPosition(), RADIUS);
}

void PlayerSensor::onContact(GameObject* other, bool foot_sensor) {
	if (other == &player) {
		return;
	}
	++player.numFootContacts;
	if (dynamic_cast<Player*>(other)) {
		player.onAnotherPlayer = true;
	}
}

void PlayerSensor::endContact(GameObject* other, bool foot_sensor) {
	if (!body || other == &player) {
		return;
	}
	--player.numFootContacts;
	if (dynamic_cast<Player*>(other)) {
		player.onAnotherPlayer = false;
	}
}
